package ch.samuelbrand.springbootopenapidemo;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class HelloControllerTest {

  @Autowired
  TestRestTemplate restTemplate;

  @Test
  void helloReturnsOk() {
    var response = restTemplate.getForEntity("/hello", String.class);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

}
